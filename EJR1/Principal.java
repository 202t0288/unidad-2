import java.util.Scanner;

public class Principal{
  public static void main(String args[]){
  
  Vehiculo myVehiculo = new Vehiculo();
  Vehiculo myVehiculo2 = new Vehiculo("Seat","1990","Rojo","ZIP-7289");
  
  Scanner input = new Scanner(System.in);

  System.out.println("Objeto 1");
  
  myVehiculo.showAtributos();
  
  System.out.println("\n Objeto 2");
   
  myVehiculo2.showAtributos();

//-------------------------------------
  System.out.println("\n\nUtilizando metodos set y get");
  System.out.println("Ingresa el nombre de la marca para objeto 1: ");
  String marca = input.nextLine();
  
  myVehiculo.setMarca(marca);
  System.out.println("Marca objeto1: " + myVehiculo.getMarca());

  System.out.println("Ingresa el nombre de la marca para objeto 2: ");
  String marca2 = input.nextLine();
  
  myVehiculo2.setMarca(marca2);
  System.out.println("Marca objeto2: " + myVehiculo2.getMarca());


  
  }

}