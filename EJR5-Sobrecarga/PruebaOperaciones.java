public class PruebaOperaciones{

  public static void main(String args[]){
  
  Operaciones operacion1 = new Operaciones();
  Operaciones operacion2 = new Operaciones(5,7);
 
   System.out.println("Valor de a: "+ operacion1.getA());
   System.out.println("Valor de b: "+ operacion1.getB());
    System.out.println("------------");
   System.out.println("Valor de a: "+ operacion2.getA());
   System.out.println("Valor de b: "+ operacion2.getB());
   
   System.out.println("La suma con el metodo1: " +operacion1.suma(2,3));
   System.out.println("La suma con el metodo2: " +operacion1.suma(2.0,7.0));
   System.out.println("La suma con el metodo3: " +operacion1.suma(2,3,5));
  

  
  }
}