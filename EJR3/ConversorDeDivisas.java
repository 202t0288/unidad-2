class ConversorDeDivisas {

// atributo de la clase
  private double tipoCambio;

// Constructor por defecto
  public ConversorDeDivisas(){
   tipoCambio = 0;
  }

// Constructor con parametros
  public ConversorDeDivisas(double tipoCambio){
   this.tipoCambio = tipoCambio;
  }

// metodo get y set

 public void setTipoCambio(double tc){
 tipoCambio = tc;
 }
 
 public double getTipoCambio(){
 return tipoCambio;
 }


// metodos

public double pesosToEuros(double valorPesos){
  return valorPesos / tipoCambio;
}

public double eurosToPesos(double valorEuro){
  return valorEuro * tipoCambio;
}

public double pesosToDolar(double valorPesos){
  return valorPesos / tipoCambio;
}

public double dolarToPesos(double valorDls){
  return valorDls * tipoCambio;
}

}