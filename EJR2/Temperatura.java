class Temperatura{

private double tempF;
private double tempC;

// metodos set
 public void setTempCelcius(double tempC){
  this.tempC = tempC;
 }
 
 public void setTempFarenheit(double tempF){
  this.tempF = tempF;
 }
 
 // metodos get
 public double getTempCelcius(){
 return tempC;
 }
 
 public double getTempFarenheit(){
 return tempF;
 }
 
 // metodos de conversion
 public double celsius_Farenheit(){
 return (1.8 * tempC) +32;
 }
 
 public double farenheit_Celsius(){
 return (tempF - 32) / 1.8;
 }
 
 //------------Metodo para  evaluar en metodo--------------------
 public double conversion(int x){
 
 if(x ==1)
   return (1.8 * tempC) +32;
 else
   return (tempF - 32) / 1.8;

 }
 //--------------------------------
}